# Area A task 4 self-assembly
This is the area to work on self-assembly synthesis definition.
The application definition describes how one can produce samples
formed primarily by non-covalent, weak interactions (up to
hydrogeb bonds). Chemical reactions are also part of the process,
but not the primary driving force.

The definition includes a couple of possible preparation steps,
such as:
* coating techniques
 * spin coating
 * dip coating
 * drop casting
* separation steps
 * filtration
 * centrifugation
 * electrophoresis
* etc.

All these steps are individual experimental methods, which require
their own base classes to be drafted.
A first version is built up here, to be moved around with time to
a better, common repo.
