# AreaA-Synthesis-AppDef

NOTE: the repo was moved to https://github.com/FAIRmat-Experimental/Area_A_application_definitions

This repo is not updated anymore 

Working area for the Area A to develop metadata information for synthesis.

In order to describe chemical synthesis, we have to work out the base classes defining the individual steps, and application definitions to describe individual processes.
The chosen form is:
 - YAML for the human readable part
 - NEXUS XML for machine readable part
